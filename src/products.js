
var index = 1;

function loadProducts() {

	document.addEventListener('DOMContentLoaded', function(event) {

		fetch('../src/data.json')
		.then( results => results.json())
		.then( data => {

			for (var i = 0; i < data.carouselData.length; i++) {
				var recommendedProduct = data.carouselData[i];

				//Get ul tag and create li for each product
				var ul = document.getElementById("product-list");
				var li = document.createElement("li");
				li.className = "recommendation";
				li.setAttribute("product-id",recommendedProduct.code);
				li.setAttribute("style","display:none");

				//Product wrapper div
				var productDiv = li.appendChild(document.createElement("div"));
				productDiv.appendChild(document.createElement("div")).className = "product-image";

				//Product Image
				var image = productDiv.appendChild(document.createElement("a"));
				image.setAttribute("href",recommendedProduct.url);
				var imageUrl = image.appendChild(document.createElement("img"));
				imageUrl.setAttribute("src",recommendedProduct.productImageUrl);

				//Product Details
				var detailsDiv = productDiv.appendChild(document.createElement("div"));
				detailsDiv.className = "product-details";
				var productDetails = detailsDiv.appendChild(document.createElement("a"));
				productDetails.setAttribute("alt",recommendedProduct.productImageAltText);
				productDetails.setAttribute("href",recommendedProduct.url);
				var productName = productDetails.appendChild(document.createElement("h2"));
				productName.className = "product-name";
				productName.innerHTML = recommendedProduct.name; //do this
				var units = productDetails.appendChild(document.createElement("p"));
				units.className = "sales-unit";
				units.innerHTML = recommendedProduct.salesUnit
				var price = productDetails.appendChild(document.createElement("p"));
				price.className = "price";
				// price.innerHTML = recommendedProduct.price.currency + " " + recommendedProduct.price.formattedValue
				price.innerHTML = "£" + recommendedProduct.price.formattedValue

				//Button
				var button = detailsDiv.appendChild(document.createElement("button"));
				button.setAttribute("type","button");
				button.className = "buy-button";
				button.innerHTML ="Buy Now"; 

				//Append each li to ul
				ul.appendChild(li);

				//Dynamically add dots
				var scrollDots = document.getElementById("scroll");
				var dot = scrollDots.appendChild(document.createElement("span"));
				dot.setAttribute("class","dot");
				var startSlide =  i + 1;
				dot.setAttribute("onclick","activeSlide(" + startSlide +")");

			}
			showSlide(index);
		})
	});
}


//Previous and Next controls
function sliderControl(n) {
	showSlide(index += n);
}

//Slide being viewed
function activeSlide(n) {
	showSlide(index = n);
}

//Display slide
function showSlide(n) {
	var slides = document.getElementsByClassName("recommendation");
	var dots = document.getElementsByClassName("dot");
	
	if (n > slides.length) {
		index = 1;
	} else if(n < 1) {
		index = slides.length;
	}

	for (var i = 0; i < slides.length; i++) {
		slides[i].style.display = "none";
	}
	for (var j = 0; j < dots.length; j++) {
		dots[j].className = dots[j].className.replace("active", "");
	}

	slides[index-1].style.display = "block";
	dots[index-1].className += " active";
}

