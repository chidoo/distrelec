module.exports = function (grunt) {

  // start of grunt tasks
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    watch:{
      sass: {
        files: {
          '/src/css/related-products.css': '/src/css/products.scss'
        },
        options:{
          livereload:35729
        }
      },
      uglify:{
        files: '/src/products.js',
        options:{
          livereload:true
        }
      }
    },

    uglify:{
      build:{
        files:{
          '/src/products.min.js' : ['/src/products.js']
        }
      }
    },

    cssmin:{
      build:{
        src: '/src/css/related-products.css',
        dest: '/src/css/related-products.min.css'
      }
    },

    sass:{
      dev:{
        files: {
          '/src/css/related-products.css': '/src/css/products.scss'
        }
      }
    }

  })



  grunt.loadNpmTasks('grunt-contrib-watch')
  grunt.loadNpmTasks('grunt-contrib-sass')
  grunt.loadNpmTasks('grunt-contrib-cssmin')
  grunt.loadNpmTasks('grunt-contrib-uglify')
  grunt.loadNpmTasks('grunt-contrib-concat')

};