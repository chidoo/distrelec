
# *Distrelec Technical Test - Front End Engineer*
## Clone Repo
### Navigate to the root of the folder and start up server using:
### Check version of python using CLI: ``` python -V ```
- #### Python version returned above is 3.X: ``` python3 -m http.server 8080 ```
**OR**
- #### Python version returned above is 2.X: ``` python -m SimpleHTTPServer 8080 ```
## Webpage

- ### Webpage should be visible using unless port is changed:
 [http://www.localhost:8080/dist/index.html](http://www.localhost:8080/dist/index.html)

